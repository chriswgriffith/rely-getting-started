import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { validateUSState } from '../../validators/usstate.validator';
import { Location } from '../../providers/location';
import { InstallStatus } from '../../providers/installStatus';
import { GsStatusPage } from '../gs-status/gs-status';

@Component({
  selector: 'page-gs-create-location',
  templateUrl: 'gs-create-location.html'
})
export class GsCreateLocationPage {
  title: string = '';
  locationInfo: FormGroup;
  timeoutId: number;
  headerIcon: string = 'md-arrow-back';

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, public location: Location, public installStatus: InstallStatus) {
    this.title = 'Add Location';

    if (installStatus.getLocationStatusCode() == 2) {
      this.title = 'Edit Location';
    }

    console.log(installStatus.getLocationStatusCode());

    this.locationInfo = this.formBuilder.group({
      name: [location.name, Validators.required],
      address: [location.address, Validators.required],
      address2: [location.address2],
      city: [location.city, Validators.required],
      state: [location.stateRegion, [Validators.required, validateUSState]],
      zipcode: [location.zipCode, [CustomValidators.range([0, 99999]), Validators.minLength(5)]],
      phone: [location.phoneNumber, [CustomValidators.phone()]]
    });
  }

  //Validators.pattern('[A-Za-z]{5}')

  returnToStatusScreen() {
    this.navCtrl.push(GsStatusPage, { step: 2 });
  }

  createLocation() {

    //Save Data to local Object
    this.location.name = this.locationInfo.value.name;
    this.location.address = this.locationInfo.value.address;
    this.location.address2 = this.locationInfo.value.address2;
    this.location.city = this.locationInfo.value.city;
    this.location.stateRegion = this.locationInfo.value.state;
    this.location.zipCode = this.locationInfo.value.zipcode;
    this.location.phoneNumber = this.locationInfo.value.phone;

    this.headerIcon = '';
    this.title = '';

    //TBD
    //Save to server

    //Actually set once we successfully save to server
    this.installStatus.setLocationStatusCode(1);

    this.timeoutId = setTimeout(() => {
      clearTimeout(this.timeoutId);
      this.installStatus.setLocationStatusCode(2);
      this.navCtrl.push(GsStatusPage, { step: 2 });
    }, 2000);
  }
}
