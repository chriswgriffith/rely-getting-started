import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { InstallStatus } from '../../providers/installStatus';
import { GsStatusPage } from '../gs-status/gs-status';

@Component({
  selector: 'page-gs-configure-system',
  templateUrl: 'gs-configure-system.html'
})
export class GsConfigureSystemPage {

  timeoutId: number;
  title: string = 'Configure System';
  headerIcon: string = 'md-arrow-back';

  constructor(public navCtrl: NavController, public navParams: NavParams, public installStatus: InstallStatus) { }

  returnToStatusScreen() {
    this.navCtrl.push(GsStatusPage, { step: 3 });
  }

  doConfigureSystem() {

    this.installStatus.setSystemStatusCode(1);
    this.title = '';
    this.headerIcon = '';

    this.timeoutId = setTimeout(() => {
      clearTimeout(this.timeoutId);
      this.installStatus.setSystemStatusCode(2);
      this.navCtrl.push(GsStatusPage, { step: 3 });
    }, 2000);
  }
}
