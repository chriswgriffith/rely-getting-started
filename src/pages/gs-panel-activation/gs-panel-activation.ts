import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { GsScanPanelLoadingPage } from '../gs-scan-panel-loading/gs-scan-panel-loading';

@Component({
  selector: 'page-gs-panel-activation',
  templateUrl: 'gs-panel-activation.html'
})
export class GsPanelActivationPage {
  panelID:string = '123-456-ABC'; //TEMP
  

  activationCode: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder) {
    this.activationCode = this.formBuilder.group({
      code: ['', [Validators.required, Validators.minLength(6)]]
    });

  }
  
  activationCodeInput() {
    console.log(this.activationCode);
    this.navCtrl.push(GsScanPanelLoadingPage, {activationCode: this.activationCode.value.code});
  }

}
