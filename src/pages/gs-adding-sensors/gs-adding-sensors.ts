import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { InstallStatus } from '../../providers/installStatus';
import { GsStatusPage } from '../gs-status/gs-status';

@Component({
  selector: 'page-gs-adding-sensors',
  templateUrl: 'gs-adding-sensors.html'
})
export class GsAddingSensorsPage {
  timeoutId: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, public installStatus:InstallStatus) {
    this.timeoutId = setTimeout(() => {
      clearTimeout(this.timeoutId);
      this.installStatus.setSensorsStatusCode(2);
    
      this.navCtrl.push(GsStatusPage, { step: 4 });
    }, 2000);
  }

  cancelSensorAdd() {

  }
}
