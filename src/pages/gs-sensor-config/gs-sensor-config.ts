import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Sensors } from '../../providers/sensors';
import { Sensor } from '../../interfaces/sensor';
import { GsSensorInstallPage } from '../gs-sensor-install/gs-sensor-install';

@Component({
  selector: 'page-gs-sensor-config',
  templateUrl: 'gs-sensor-config.html'
})
export class GsSensorConfigPage {
  sensor: Sensor;
  saveStatus: number = 0;
  saveTimerId: number;


  constructor(public navCtrl: NavController, public navParams: NavParams, public sensors: Sensors) {
    this.sensor = navParams.get('sensor');
    console.log(this.sensor);
    //this.sensors.updateSensor(this.sensor);
  }

  ionViewDidLoad() {
    this.saveTimerId = setTimeout(() => {
      clearTimeout(this.saveTimerId);
      let theResult = this.sensors.updateSensor(this.sensor);

      if (theResult) {
        this.saveStatus = 1;
      } else {
        this.saveStatus = 2;
      }
      //this.navCtrl.push(GsStatusPage, { step: 2 });
    }, 2000);
  }

  doSensorInstall() {
     this.navCtrl.push(GsSensorInstallPage, { sensor: this.sensor });
  }

  doSkipSensorInstall() {
    console.log('skip');
  }
}
