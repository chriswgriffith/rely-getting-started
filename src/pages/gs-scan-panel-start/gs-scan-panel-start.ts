import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GsScanPanelManualPage } from '../gs-scan-panel-manual/gs-scan-panel-manual';

@Component({
  selector: 'page-gs-scan-panel-start',
  templateUrl: 'gs-scan-panel-start.html'
})
export class GsScanPanelStartPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  doPanelScan() {
    console.log('doPanelScan');
  }

  doManualPanelCode() {
    this.navCtrl.push(GsScanPanelManualPage);
  }

}
