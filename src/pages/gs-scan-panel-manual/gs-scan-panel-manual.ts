import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { GsPanelActivationPage } from '../gs-panel-activation/gs-panel-activation';

@Component({
  selector: 'page-gs-scan-panel-manual',
  templateUrl: 'gs-scan-panel-manual.html'
})
export class GsScanPanelManualPage {
  accountID: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder) {
    this.accountID = this.formBuilder.group({
      accountNumber: ['', [Validators.required, CustomValidators.range([0, 99999999]), Validators.minLength(8)]]
    });
  }

  manualCodeInput() {
    // console.log(this.accountNumber);
    this.navCtrl.push(GsPanelActivationPage, {accountNumber: this.accountID.value.accountNumber});
  }
}
