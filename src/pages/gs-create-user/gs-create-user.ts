import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { User } from '../../providers/user';
import { InstallStatus } from '../../providers/installStatus';
import { GsStatusPage } from '../gs-status/gs-status';

@Component({
  selector: 'page-gs-create-user',
  templateUrl: 'gs-create-user.html'
})
export class GsCreateUserPage {
  title:string = '';
  userInfo: FormGroup;
  timeoutId: number;
   headerIcon:string ='md-arrow-back';

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, public user: User, public installStatus: InstallStatus) {
    this.title = 'Create User';

    if (installStatus.getUserStatusCode() == 2) {
       this.title = 'Edit User';
    }

    let passwordMask:string = '*'.repeat(user.password.length);

    this.userInfo = this.formBuilder.group({
      firstName: [user.firstName, [Validators.required ]],
      lastName: [user.lastName, Validators.required],
      phone: [user.phone, [Validators.required, CustomValidators.phone()]],
      email: [user.email, [Validators.required, CustomValidators.email ]],
      username: [user.username, Validators.required],
      password: [passwordMask, [Validators.required, CustomValidators.rangeLength([5, 15])]],
      accessCode: [user.accessCode, [Validators.required, CustomValidators.digits, CustomValidators.rangeLength([4, 4])]],
      passphrase: [user.passphrase, Validators.required],
      pushNotification: [user.pushNotification, Validators.required],
      emailNotification: [user.emailNotification, Validators.required],
      textNotification: [user.textNotification, Validators.required],
    });
  }

  returnToStatusScreen() {
    this.navCtrl.push(GsStatusPage, { step: 1 });
  }

  createUser() {
    //Save Data to local Object
    this.user.firstName = this.userInfo.value.firstName;
    this.user.lastName = this.userInfo.value.lastName;
    this.user.phone = this.userInfo.value.phone;
    this.user.email = this.userInfo.value.email;
    this.user.username = this.userInfo.value.username;
    this.user.password = this.userInfo.value.password;
    this.user.accessCode = this.userInfo.value.accessCode;
    this.user.passphrase = this.userInfo.value.passphrase;
    this.user.pushNotification = this.userInfo.value.pushNotification;
    this.user.emailNotification = this.userInfo.value.emailNotification;
    this.user.textNotification = this.userInfo.value.textNotification;

    this.headerIcon = '';
    this.title = '';

    //TBD
    //Save to server

    //Actually set once we successfully save to server
    this.installStatus.setUserStatus(1);
 

    this.timeoutId = setTimeout(() => {
      clearTimeout(this.timeoutId);
      this.installStatus.setUserStatus(2);
      this.navCtrl.push(GsStatusPage, { step: 1 });
    }, 2000);
  }

}