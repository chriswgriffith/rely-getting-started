import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { GsScanPanelStartPage } from '../gs-scan-panel-start/gs-scan-panel-start';
import { InstallStatus } from '../../providers/installStatus';
import { Sensors } from '../../providers/sensors';

//Dev Shortcut
import { GsScanPanelLoadingPage } from '../gs-scan-panel-loading/gs-scan-panel-loading';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, public sensors: Sensors, public installStatus:InstallStatus) {
    //TEMP
    sensors.initSensorArray();
    // installStatus.setUserStatus(2);
    // installStatus.setLocationStatusCode(2);
    // installStatus.setSystemStatusCode(2);
    // installStatus.setSensorsStatusCode(2);
    
  }

  doGettingStarted() {
    //Real
    //this.navCtrl.push(GsScanPanelStartPage );
    //Dev
    this.navCtrl.push(GsScanPanelLoadingPage);
  }
}
