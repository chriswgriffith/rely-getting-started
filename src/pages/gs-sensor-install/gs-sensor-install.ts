import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Sensor } from '../../interfaces/sensor';
import { GsSensorTestPage } from '../gs-sensor-test/gs-sensor-test';
import { GsSensorListPage } from '../gs-sensor-list/gs-sensor-list';


@Component({
  selector: 'page-gs-sensor-install',
  templateUrl: 'gs-sensor-install.html'
})
export class GsSensorInstallPage {
  sensor: Sensor;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.sensor = navParams.get('sensor');
  }

  doInstallContinue() {
    this.navCtrl.push(GsSensorTestPage, { sensor: this.sensor });
  }

  skipInstallerTest() {
    this.navCtrl.push(GsSensorListPage);
  }

}
