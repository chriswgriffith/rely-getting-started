import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Content } from 'ionic-angular';
import { Slides } from 'ionic-angular';
import { User } from '../../providers/user';
import { Location } from '../../providers/location';
import { InstallStatus } from '../../providers/installStatus';
import { GsCreateUserPage } from '../gs-create-user/gs-create-user';
import { GsCreateLocationPage } from '../gs-create-location/gs-create-location';
import { GsConfigureSystemPage } from '../gs-configure-system/gs-configure-system';
import { GsSensorListPage } from '../gs-sensor-list/gs-sensor-list';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-gs-status',
  templateUrl: 'gs-status.html'
})
export class GsStatusPage {
  startSlide: number = 0;
  headerIcon: string = 'md-close';
  @ViewChild(Slides) slides: Slides;
  @ViewChild(Content) content: Content;

  constructor(public navCtrl: NavController, public navParams: NavParams, public user: User, public location: Location, public installStatus: InstallStatus) {
    let returnSlide = this.navParams.get('step');
    if (returnSlide !== undefined) {
      this.headerIcon = 'menu';
      this.startSlide = returnSlide;
      //this.mySlideOptions.initialSlide = returnSlide;
    }

  }

  ngAfterViewInit() {
    this.content.resize();
    //this.slides.initialSlide = this.startSlide;
    //this.slides.update();
    //setTimeout(() =>{  this.slides.update(); console.log('lw');   }, 2000);
  }

  cancelSetup() {
    if (this.slides.getActiveIndex() === 0) {
      this.navCtrl.popToRoot();
    } else {
      this.slides.slideTo(0, 250);
    }
  }

  updateSlide(evt: any) {
    if (this.slides.getActiveIndex() === 0) {
      this.headerIcon = 'md-close';
    } else {
      this.headerIcon = 'menu';
    }
  }

  /////
  //First slide nav
  goSetupStep() {
    this.slides.slideTo(1, 250);
  }
  //////
  //List Nav
  goUserSlide() {
    this.headerIcon = 'menu';
    this.slides.slideTo(1, 250);
  }

  goLocationSlide() {
    this.headerIcon = 'menu';
    this.slides.slideTo(2, 250);
  }

  goSystemSlide() {
    this.headerIcon = 'menu';
    this.slides.slideTo(3, 250);
  }

  goSensorsSlide() {
    this.headerIcon = 'menu';
    this.slides.slideTo(4, 250);
  }

  ///////
  //Detail Screen nav
  goCreateUserScreen() {
    this.navCtrl.push(GsCreateUserPage);
  }

  goCreateLocation() {
    this.navCtrl.push(GsCreateLocationPage);
  }

  goConfigureSystem() {
    this.navCtrl.push(GsConfigureSystemPage);
  }

  goConfigureSensors() {
    this.navCtrl.push(GsSensorListPage);
  }
}
