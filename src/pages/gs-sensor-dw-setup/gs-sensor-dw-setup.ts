import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NavController, NavParams } from 'ionic-angular';
import { Sensors } from '../../providers/sensors';
import { Sensor } from '../../interfaces/sensor';
import { GsSensorConfigPage } from '../gs-sensor-config/gs-sensor-config';
import { GsSensorListPage } from '../gs-sensor-list/gs-sensor-list';

@Component({
  selector: 'page-gs-sensor-dw-setup',
  templateUrl: 'gs-sensor-dw-setup.html'
})
export class GsSensorDWSetupPage {
  sensorConfig: FormGroup;
  sensor: Sensor;
  locationNames: Array<Object> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, public sensors: Sensors) {
    this.sensor = navParams.get('sensor');

    let locationName: string = '';
    if (this.sensor.location) {
      locationName = this.sensor.name;
    }

    this.sensorConfig = this.formBuilder.group({
      sensorType: ['door', Validators.required],
      locationName: [locationName, Validators.required],
      isExit: [this.sensor.isExit]
    });

    this.locationNames = [{ "id": 0, "name": "Back Door" },
    { "id": 1, "name": "Basement Door" },
    { "id": 2, "name": "Exit Door" },
    { "id": 3, "name": "Front Door" },
    { "id": 4, "name": "Garage Door" },
    { "id": 5, "name": "Main Door" },
    { "id": 6, "name": "Outside Door" },
    { "id": 7, "name": "Patio Door" },
    { "id": 8, "name": "Shed Door" },
    { "id": 9, "name": "Shop Door" },
    { "id": 10, "name": "Sliding Door" }];
  }

  cancelSensorSetup() {
    this.navCtrl.push(GsSensorListPage);
  }

  selectedDoor() {
    this.sensorConfig.patchValue({ locationName: "" });

    this.locationNames = [{ "id": 0, "name": "Back Door" },
    { "id": 1, "name": "Basement Door" },
    { "id": 2, "name": "Exit Door" },
    { "id": 3, "name": "Front Door" },
    { "id": 4, "name": "Garage Door" },
    { "id": 5, "name": "Main Door" },
    { "id": 6, "name": "Outside Door" },
    { "id": 7, "name": "Patio Door" },
    { "id": 8, "name": "Shed Door" },
    { "id": 9, "name": "Shop Door" },
    { "id": 10, "name": "Sliding Door" }];
  }

  selectedWindow() {
    this.sensorConfig.patchValue({ locationName: "" });

    this.locationNames = [{ "id": 0, "name": "Back Window" },
    { "id": 1, "name": "Basement Window" },
    { "id": 2, "name": "Bathroom Window" },
    { "id": 3, "name": "Bedroom Window" },
    { "id": 4, "name": "Dining Room Window" },
    { "id": 5, "name": "Family Room Window" },
    { "id": 6, "name": "Front Window" },
    { "id": 7, "name": "Guest Room Window" },
    { "id": 8, "name": "Kitchen Window" },
    { "id": 9, "name": "Master Bedroom Window" },
    { "id": 10, "name": "Office Window" },
    { "id": 11, "name": "Side Window" },
    { "id": 12, "name": "Utility Room Window" }];
  }

  configSensor() {
    this.sensor.subtype = this.sensorConfig.value.sensorType;
    this.sensor.name = this.sensorConfig.value.locationName;
    this.sensor.isExit = this.sensorConfig.value.isExit;
    this.sensor.location = true;
    this.navCtrl.push(GsSensorConfigPage, { sensor: this.sensor });
  }

}
