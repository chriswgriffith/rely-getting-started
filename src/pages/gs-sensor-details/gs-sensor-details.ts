import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Sensor } from '../../interfaces/sensor';
import { GsSensorDWSetupPage }  from '../gs-sensor-dw-setup/gs-sensor-dw-setup';
import { GsSensorSetupPage } from '../gs-sensor-setup/gs-sensor-setup';

@Component({
  selector: 'page-gs-sensor-details',
  templateUrl: 'gs-sensor-details.html'
})
export class GsSensorDetailsPage {
  sensor:Sensor;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
      this.sensor = navParams.get('sensor');
      console.log(this.sensor);
  }

  goSetupStep() {
    if (this.sensor.type  === 'door_window' ) {
      this.navCtrl.push(GsSensorDWSetupPage, {sensor:this.sensor });
    } else {
      this.navCtrl.push(GsSensorSetupPage, {sensor:this.sensor });
    }
  }
  
}
