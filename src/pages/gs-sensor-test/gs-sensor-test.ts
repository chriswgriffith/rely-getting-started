import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { InstallStatus } from '../../providers/installStatus';
import { Sensor } from '../../interfaces/sensor';
import { Sensors } from '../../providers/sensors';
import { GsSensorListPage } from '../gs-sensor-list/gs-sensor-list';


@Component({
  selector: 'page-gs-sensor-test',
  templateUrl: 'gs-sensor-test.html'
})
export class GsSensorTestPage {
  sensor: Sensor;

  constructor(public navCtrl: NavController, public navParams: NavParams, public sensors: Sensors, public installStatus:InstallStatus) {
    this.sensor = navParams.get('sensor');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GsSensorTestPage');
  }

  doSensorTestComplete() {
    this.sensor.status = 0;
    this.sensors.updateSensor(this.sensor);

    this.navCtrl.push(GsSensorListPage);
  }

  doSensorTestCancel() {
    this.sensor.status = 1;  //Incomplete
    this.sensors.updateSensor(this.sensor);
    this.navCtrl.push(GsSensorListPage);
  }

}
