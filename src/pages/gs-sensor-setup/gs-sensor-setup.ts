import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NavController, NavParams } from 'ionic-angular';
import { Sensors } from '../../providers/sensors';
import { Sensor } from '../../interfaces/sensor';
import { GsSensorConfigPage } from '../gs-sensor-config/gs-sensor-config';
import { GsSensorListPage } from '../gs-sensor-list/gs-sensor-list';

@Component({
  selector: 'page-gs-sensor-setup',
  templateUrl: 'gs-sensor-setup.html'
})
export class GsSensorSetupPage {
  sensorConfig: FormGroup;
  sensor: Sensor;
  locationNames: Array<Object> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, public sensors: Sensors) {
    this.sensor = navParams.get('sensor');

    let locationName: string = '';
    if (this.sensor.location) {
      locationName = this.sensor.name;
    }

    this.sensorConfig = this.formBuilder.group({
      locationName: [locationName, Validators.required],
    });

    this.locationNames = [{ "id": 0, "name": "Basement" },
    { "id": 1, "name": "Bathroom" },
    { "id": 2, "name": "Bedroom" },
    { "id": 3, "name": "Dining Room" },
    { "id": 4, "name": "Downstairs Hallway" },
    { "id": 5, "name": "Extra Room" },
    { "id": 6, "name": "Family Room" },
    { "id": 7, "name": "Front Room" },
    { "id": 8, "name": "Garage" },
    { "id": 9, "name": "Guest Room" },
    { "id": 0, "name": "Kitchen" },
    { "id": 10, "name": "Living Room" },
    { "id": 11, "name": "Master Bedroom" },
    { "id": 12, "name": "Office" },
    { "id": 13, "name": "Upstairs Hallway" },
    { "id": 14, "name": "Utility Room" }
    ];
  }

  cancelSensorSetup() {
    this.navCtrl.push(GsSensorListPage);
  }


  configSensor() {
    this.sensor.name = this.sensorConfig.value.locationName;
    this.sensor.isExit = false;

    this.navCtrl.push(GsSensorConfigPage, { sensor: this.sensor });
  }


}
