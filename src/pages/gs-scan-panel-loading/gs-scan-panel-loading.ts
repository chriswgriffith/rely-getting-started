import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GsStatusPage }  from '../gs-status/gs-status';
import { GsPanelActivationPage } from '../gs-panel-activation/gs-panel-activation';

@Component({
  selector: 'page-gs-scan-panel-loading',
  templateUrl: 'gs-scan-panel-loading.html'
})
export class GsScanPanelLoadingPage {
  timeoutId: number;

  constructor(public navCtrl: NavController, public navParams: NavParams) { }

  cancelSetupLoad() {
    clearTimeout(this.timeoutId);
    this.navCtrl.push(GsPanelActivationPage);
  }

  ionViewDidLoad() {
    this.timeoutId = setTimeout(() => {
      clearTimeout(this.timeoutId);
      this.navCtrl.push(GsStatusPage);
    }, 2000);
  }
}
