import { Component } from '@angular/core';
import { NavController, NavParams, ItemSliding } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { InstallStatus } from '../../providers/installStatus';
import { Sensors } from '../../providers/sensors';
import { Sensor } from '../../interfaces/sensor';
import { GsSensorDetailsPage } from '../gs-sensor-details/gs-sensor-details';
import { GsAddingSensorsPage } from '../gs-adding-sensors/gs-adding-sensors';

@Component({
  selector: 'page-gs-sensor-list',
  templateUrl: 'gs-sensor-list.html'
})
export class GsSensorListPage {
  title: string = '';
  userSensors: Array<Sensor> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public sensors: Sensors, public installStatus: InstallStatus) {
    this.title = 'Configure Sensors';
    this.userSensors = this.sensors.getSensors();
  }

  goSensorDetails(theSensor: Object) {
    this.navCtrl.push(GsSensorDetailsPage, { sensor: theSensor });
  }

  deleteSensor(slidingItem: ItemSliding, theSensor: Sensor) {
    console.log(theSensor);
    slidingItem.close();

    let alert = this.alertCtrl.create({
      title: 'Delete Sensor',
      subTitle: 'Do you want to remove the ' + theSensor.name + ' sensor?',
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('here', theSensor);
            if (this.sensors.removeSensor(theSensor)) {
              this.userSensors = this.sensors.getSensors();
            } else {
               console.log('unable to delete');
            }
          }
        },
        { text: 'Cancel' }
      ]
    });
    alert.present();
  }

  addNewSensor() {

  }

  addAllSensors() {
    this.navCtrl.push(GsAddingSensorsPage);
  }
}