export class InstallStatus {
  public userStatus: number = 0; //0 not complete; 1 Creating; 2 Created
  public locationStatus: number = 0; //0 not complete
  public systemStatus: number = 0;
  public sensorStatus: number = 0;

  constructor() { }

  getStatusCode():number {
    let theStatusCode:number = 0;

    if ( (this.userStatus == 2) && (this.locationStatus == 2) && (this.systemStatus == 2) && (this.sensorStatus == 2) ) {
      theStatusCode = 2;
    }

    return theStatusCode;
  }

  getStatus():string {
    if ( this.getStatusCode() == 0){
      return 'Not Complete';
    } else if (this.getStatusCode() == 1 ) {
      return 'Finishing…';
      } else if (this.getStatusCode() == 2 ) {
        return 'Complete';
    } else {
      return '';
    }
  }

  getUserStatus(): string {
    if (this.userStatus == 0) {
      return 'Not Complete';
    } else if (this.userStatus == 1) {
      return 'Creating';
    } else if (this.userStatus == 2) {
      return 'Created';
    } else {
      return '';
    }
  }

  getUserStatusCode(): number {
    return this.userStatus;
  }

  setUserStatus(theStatus: number) {
    this.userStatus = theStatus;
  }

  getLocationStatus(): string {
    if (this.locationStatus == 0) {
      return 'Not Complete';
    } else if (this.locationStatus == 1) {
      return 'Creating';
    } else if (this.locationStatus == 2) {
      return 'Created';
    } else {
      return '';
    }
  }

  getLocationStatusCode(): number {
    return this.locationStatus;
  }

  setLocationStatusCode(theStatus: number) {
    this.locationStatus = theStatus;
  }

  getSystemStatus(): string {
    if (this.systemStatus == 0) {
      return 'Not Complete';
    } else if (this.systemStatus == 1) {
      return 'Creating';
    } else if (this.systemStatus == 2) {
      return 'Created';
    } else {
      return '';
    }
  }

  getSystemStatusCode(): number {
    return this.systemStatus;
  }

  setSystemStatusCode(theStatus: number) {
    this.systemStatus = theStatus;
  }

  getSensorStatus(): string {
    if (this.sensorStatus == 0) {
      return 'Not Complete';
    } else if (this.sensorStatus == 1) {
      return 'Creating';
    } else if (this.sensorStatus == 2) {
      return 'Created';
    } else {
      return '';
    }
  }

  getSensorsStatusCode(): number {
    return this.sensorStatus;
  }

  setSensorsStatusCode(theStatus: number) {
    this.sensorStatus = theStatus;
  }


}