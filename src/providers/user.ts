export class User {
  public firstName: string = '';
  public lastName: string = '';

  public phone: string = '';
  public email: string = '';
  public avatar: string = '';
  public city: string = '';
  public stateRegion: string = '';
  public country: string = '';
  public countryCode: string = '';

  public username: string = '';
  public password: string = '';
  public accessCode: string = '';
  public passphrase: string = '';
  public pushNotification: boolean = true;
  public emailNotification:  boolean = true;
  public textNotification: boolean = true;
 

  constructor() {

  }

  getFullName(): string {
    return this.firstName + ' ' + this.lastName;
  }
}

//TO DO
//This is just the start of the proper User class
