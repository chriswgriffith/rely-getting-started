export class Location {
  
  public name: string = '';

  public address: string = '';
  public address2: string = '';
  public city: string = '';
  public stateRegion: string = '';
  public countryCode: string = '';
  public zipCode: string = '';
  public phoneNumber: string = '';
  public photoURI: string = '';


  constructor() {
   
  }
}

//TO DO
//This is just the start of the proper Location class
