import { Sensor } from '../interfaces/sensor';

export class Sensors {
  public sensors: Array<Sensor> = [];

  constructor() { }

  initSensorArray() {
    this.sensors = [];
    //TEMP
    //console.log('initSensorArray');
    this.sensors.push({ "type": "door_window", "subtype": "door", "name": "Front Door", "TXID": "1234-6678", "status": 0, "location": true, "isExit": false });
    this.sensors.push({ "type": "door_window", "subtype": "", "name": "Door/Window #1", "TXID": "1234-2112", "status": 0, "location": false, "isExit": true });
    this.sensors.push({ "type": "motion", "subtype": "", "name": "Motion #1", "TXID": "1234-6673", "status": 0, "location": false });
  }

  getSensors() {
    return this.sensors;
  }

  addSensor(theSensorData: Sensor) {
    //TODO
  }

  updateSensor(theSensorData: Sensor): Boolean {
    let theResult: Boolean = false;
    for (let sensor of this.sensors) {
      if (sensor.TXID === theSensorData.TXID) {
        sensor = theSensorData

        theResult = true;
      }
    }
    return theResult;
  }

  getSensorStatus(): Boolean {
    let theResult: Boolean = true;
    for (let sensor of this.sensors) {
      if ((sensor.status == 1) || (sensor.status == 2)) {
        theResult = false;
      }
    }

    return theResult;
  }

  removeSensor(theSensor:Sensor):Boolean {
    let theResult = false;

    for (let i:number = 0; i < this.sensors.length; i++) {
      if (this.sensors[i].TXID === theSensor.TXID) {
        this.sensors.splice(i,1);
        theResult = true;
        break;
      }

    }
    
    return theResult;
  }

}
