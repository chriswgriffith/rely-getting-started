import { NgModule, ErrorHandler } from '@angular/core';
import { TranslateModule, TranslateLoader, TranslateStaticLoader } from 'ng2-translate/ng2-translate';
import { Globalization } from 'ionic-native';
import { Http } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { User } from '../providers/user';
import { Location } from '../providers/location';
import { InstallStatus } from '../providers/installStatus';
import { Sensors } from '../providers/sensors';

import { SensorIcon  } from '../pipes/sensor-icon';
import { SensorStatus} from '../pipes/sensor-status';

import { GsScanPanelStartPage } from '../pages/gs-scan-panel-start/gs-scan-panel-start';
import { GsScanPanelManualPage } from '../pages/gs-scan-panel-manual/gs-scan-panel-manual';
import { GsPanelActivationPage } from '../pages/gs-panel-activation/gs-panel-activation'; 
import { GsScanPanelLoadingPage }  from '../pages/gs-scan-panel-loading/gs-scan-panel-loading';
import { GsStatusPage } from '../pages/gs-status/gs-status';
import { GsCreateUserPage } from '../pages/gs-create-user/gs-create-user';
import { GsCreateLocationPage } from '../pages/gs-create-location/gs-create-location';
import { GsConfigureSystemPage } from '../pages/gs-configure-system/gs-configure-system';
import { GsSensorListPage } from '../pages/gs-sensor-list/gs-sensor-list';
import { GsSensorDetailsPage } from '../pages/gs-sensor-details/gs-sensor-details';
import { GsSensorDWSetupPage } from '../pages/gs-sensor-dw-setup/gs-sensor-dw-setup';
import { GsSensorSetupPage } from '../pages/gs-sensor-setup/gs-sensor-setup';

import { GsSensorConfigPage } from '../pages/gs-sensor-config/gs-sensor-config';
import { GsSensorInstallPage } from '../pages/gs-sensor-install/gs-sensor-install';
import { GsSensorTestPage }  from '../pages/gs-sensor-test/gs-sensor-test';
import { GsAddingSensorsPage  } from '../pages/gs-adding-sensors/gs-adding-sensors';

export function createTranslateLoader(http: Http) {
    return new TranslateStaticLoader(http, 'assets/i18n', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    GsScanPanelStartPage,
    GsScanPanelManualPage,
    GsPanelActivationPage,
    GsScanPanelLoadingPage,
    GsStatusPage,
    GsCreateUserPage,
    GsCreateLocationPage,
    GsConfigureSystemPage,
    GsSensorListPage,
    GsSensorDetailsPage,
    GsSensorDWSetupPage,
    GsSensorSetupPage,
    GsSensorConfigPage,
    GsSensorInstallPage,
    GsSensorTestPage,
    GsAddingSensorsPage,
    SensorIcon, 
    SensorStatus
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    TranslateModule.forRoot({
      provide: TranslateLoader,
      useFactory: (createTranslateLoader),
      deps: [Http]
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    GsScanPanelStartPage,
    GsScanPanelManualPage,
    GsPanelActivationPage,
    GsScanPanelLoadingPage,
    GsStatusPage,
    GsCreateUserPage,
    GsCreateLocationPage,
    GsConfigureSystemPage,
    GsSensorListPage,
    GsSensorDetailsPage,
    GsSensorConfigPage,
    GsSensorDWSetupPage,
    GsSensorSetupPage,
    GsSensorInstallPage,
    GsSensorTestPage,
    GsAddingSensorsPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}, 
  { provide: User, useClass: User }, 
  { provide: Location, useClass: Location }, 
  { provide: InstallStatus, useClass: InstallStatus },
  { provide: Sensors, useClass: Sensors }, Globalization]
})

export class AppModule {}
