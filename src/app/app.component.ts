import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar, Splashscreen, Globalization } from 'ionic-native';
import { TranslateService } from  'ng2-translate';
import { HomePage } from '../pages/home/home';
import { User } from '../providers/user';
import { Location } from '../providers/location';
import { InstallStatus } from '../providers/installStatus';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage = HomePage;

  constructor(public platform: Platform, public translateService:TranslateService, public globalization:Globalization, public user:User, public location:Location, public installStatus:InstallStatus) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();

      console.log(Globalization.getLocaleName() );
      
      translateService.use('en');
    });
  }
}
