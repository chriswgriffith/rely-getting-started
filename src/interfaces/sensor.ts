export interface Sensor {
    type: string;
    subtype: string;
    name: string;
    TXID: string;
    status: number;
    location: boolean;
    isExit?: boolean;
}

//status
//0 == set up complete
//1 == set up incomplete
//2 == error