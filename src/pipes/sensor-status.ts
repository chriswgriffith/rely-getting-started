import { Injectable, Pipe } from '@angular/core';

@Pipe({
  name: 'sensorStatus'
})
@Injectable()
export class SensorStatus {
  
  transform(value, args) {
    var iconFileName:string ='';

    if (value === 0 ) {
      iconFileName = "status-good.svg";
    } else if (value === 1 ) {
      iconFileName = "status-incomplete.svg";
    } else if (value === 2 ) {
      iconFileName = "status-error.svg";
    }
    
    return iconFileName;
  }
}
