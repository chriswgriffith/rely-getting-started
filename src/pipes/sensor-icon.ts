import { Injectable, Pipe } from '@angular/core';

@Pipe({
  name: 'sensorIcon'
})
@Injectable()
export class SensorIcon {

  transform(value, args) {
    var iconFileName:string ='';

    if (value === 'door_window' ) {
      iconFileName = "door-window-sensor.svg";
    } else if (value === 'smoke' ) {
      iconFileName = "smoke-sensor.svg";
    } else if (value === 'co' ) {
      iconFileName = "co-sensor.png";
    } else if (value === 'glass_break' ) {
      iconFileName = "glass-break-sensor.svg";
    } else if (value === 'motion' ) {
      iconFileName = "motion-sensor.svg";
    } else {
       iconFileName = "sensor.svg";
    }
    
    return iconFileName;
  }
}
